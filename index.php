<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <title>Teste do Bastos</title>
</head>
<body>
    <form>        
        <div>
            <a href="/imagem.php">Nova imagem</a>
        </div>

        <div id="images" style="display: flex; ">
        </div>
    </form>
</body>

<script>
    $(document).ready(function() {
        $.ajax({
            type: 'GET',
            url: '/controller/getImages.php',
            dataType: 'JSON',
            success: function(response) {
                    response.forEach((value) => {
                        $('#images').append(                        
                            '<div style="margin-right: 10px;">' +
                                '<img style="width: 200px; height: 200px;" src="http://localhost:3000/upload/' + value.url + '" />' +
                                '<label style="display:block;">'+ value.descricao +'</label>' +
                            '</div>'                        
                    )
                });
            },
            error: function(xhr) {
                console.log(xhr);
            }
        })
    });
</script>

</html>