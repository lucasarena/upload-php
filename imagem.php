<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <title>Teste do Bastos</title>
</head>
<body>
    <a href="/">Voltar</a>
    <form id="form" enctype="multipart/form-data">        
        <input type="file" / id="file">
        <div>
            <label for="">Descrição</label>
            <input type="text" name="descricao" id="descricao"/>
            <button type="button" id="salvar">Salvar</button>
        </div>
    </form>
</body>

<script>
    $('#salvar').click(function() {
        let form = new FormData();

        form.append('descricao', $('#descricao').val())
        form.append('file', $('#file')[0].files[0])        
        $.ajax({
            type: 'POST',
            url: '/controller/upload.php',
            data: form,
            dataType: 'JSON',
            success: function(response) {
                if (response.success) {
                    window.location.href = '/';
                }
            },
            error: function(xhr) {
                console.log(xhr)
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });
</script>

</html>